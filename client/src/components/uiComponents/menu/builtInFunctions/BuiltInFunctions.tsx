import { useContext, useEffect } from "react";
import WorkspaceStateContext from "../../../../store/workspace-state-context";

function BuiltInFunctions() {

    const workspaceStateCtx = useContext(WorkspaceStateContext);

    useEffect(function() {
        // Fetch the built-in functions file from the backend
        async function fetchBuiltInFunction() {
            // http://localhost:8082/
            // https://backend-plcverif-web-interface.app.cern.ch
            const url = "https://backend-plcverif-web-interface.app.cern.ch";

            try {
                const response = await fetch(url, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: "BUILTIN_FUNCTIONS_REQUEST"
                });

                if(response.ok) {
                    var responseData = await response.text();
                    workspaceStateCtx.setListOfBuiltInFunctions(responseData);
                }
            } catch (err) {
                console.log("ErrorBuiltInFunctions: " + err);
            }
        }

        fetchBuiltInFunction();
    }, []);

    return (
        <ul className="list-group">
            {
                workspaceStateCtx.listOfBuiltInFunctions.map((name, index) => (
                    <li key = {name + index} className="list-group-item d-flex justify-content-between align-items-center">
                        {name}
                    </li>
                ))
            }
        </ul>
    );
}

export default BuiltInFunctions;