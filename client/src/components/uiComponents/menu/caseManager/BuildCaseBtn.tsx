import { useContext } from "react";
import ColorThemeContext from "../../../../store/color-theme-context";
import CaseBuildingContext from "../../../../store/caseBuilding-context";
import WorkspaceStateContext from "../../../../store/workspace-state-context";
import CaseBuiltPopUp from "./CaseBuiltPopUp";

function BuildCaseBtn() {

    const colorThemeCtx = useContext(ColorThemeContext);
    const caseBuildingCtx = useContext(CaseBuildingContext);
    const workspaceStateCtx = useContext(WorkspaceStateContext);

    function returnCorrespondingModelVariantValues(modVar: string) {
        var valueToReturn = ""
        switch(modVar) {
            case "CFA declaration":
                valueToReturn = "CFD"
                break;
            case "CFA instance":
                valueToReturn = "CFI"
                break;
            case "Structured CFA declaration":
                valueToReturn = "CFD_STRUCTURED";
                break;
        }
        return valueToReturn;
    }

    function buildCase() {
        const mdIDRefVAL = caseBuildingCtx.metadata.id;
        const mdNameRefVAL = caseBuildingCtx.metadata.name;
        const mdDescriptionRefVAL = caseBuildingCtx.metadata.description;
        const vbBackendRefVAL = caseBuildingCtx.verificationBackend.backend;
        const vbBackendNuSMVAlgorithmRefVAL = caseBuildingCtx.verificationBackend.algorithm;
        const vbBackendCBMCESBMCModVarRefVAL = caseBuildingCtx.verificationBackend.modelVariant;
        const reqReqtypeRefVAL = caseBuildingCtx.requirement.type;
        const reqReqtypeAssCheckRefVAL = caseBuildingCtx.requirement.check;
        const reqReqtypePatPatRefVAL = caseBuildingCtx.requirement.pattern;
        const reqReqtypePat1RefVAL = caseBuildingCtx.requirement.one;
        const reqReqtypePat2RefVAL = caseBuildingCtx.requirement.two;
        const reqReqtypePat3RefVAL = caseBuildingCtx.requirement.three;
        const entryBlockRefVAL = caseBuildingCtx.sourceFiles.entryBlock;

        var verification_case = "";

        var backendSettingToAdd = ((vbBackendRefVAL.toString() == "NuSMV") ? 
            (vbBackendNuSMVAlgorithmRefVAL.toString() == "Default" ? "" : "-job.backend.algorithm = " + vbBackendNuSMVAlgorithmRefVAL + "\r\n")
            :
            (vbBackendRefVAL.toString() == "CBMC" || vbBackendRefVAL.toString() == "ESBMC") ?
                (vbBackendCBMCESBMCModVarRefVAL.toString() == "Default" ? "" :  "-job.backend.model_variant = " + returnCorrespondingModelVariantValues(vbBackendCBMCESBMCModVarRefVAL) + "\r\n")
                : ""
        );

        console.log(vbBackendNuSMVAlgorithmRefVAL)

        if(reqReqtypeRefVAL.toString() == "Pattern"){
            verification_case = 
                "-description = \"" + mdDescriptionRefVAL + "\"\r\n"
                + "-id = " + mdIDRefVAL + "\r\n"
                + "-job = verif\r\n"
                + "-job.backend = " + vbBackendRefVAL.toLowerCase() + "\r\n"
                + backendSettingToAdd
                + "-job.req = pattern\r\n"
                + "-job.req.pattern_id = " + reqReqtypePatPatRefVAL + "\r\n"
                + "-job.req.pattern_params.1 = \"" + reqReqtypePat1RefVAL + "\"\r\n"
                + "-job.req.pattern_params.2 = \"" + reqReqtypePat2RefVAL + "\"\r\n"
                + "-job.req.pattern_params.3 = \"" + reqReqtypePat3RefVAL + "\"\r\n"
                + "-lf = step7\r\n"
                + "-lf.entry = " + entryBlockRefVAL + "\r\n"
                + "-name = \"" + mdNameRefVAL + "\"\r\n"
        } else if(reqReqtypeRefVAL.toString() == "Division by zero") {
            verification_case = 
                "-description = \"" + mdDescriptionRefVAL + "\"\r\n"
                + "-id = " + mdIDRefVAL + "\r\n"
                + "-job = verif\r\n"
                + "-job.backend = " + vbBackendRefVAL.toLowerCase() + "\r\n"
                + backendSettingToAdd
                + "-job.req = divbyzero\r\n"
                + "-lf = step7\r\n"
                + "-lf.entry = " + entryBlockRefVAL + "\r\n"
                + "-name = \"" + mdNameRefVAL + "\"\r\n"
        } else if(reqReqtypeRefVAL.toString() == "Assertion") {
            verification_case = 
                "-description = \"" + mdDescriptionRefVAL + "\"\r\n"
                + "-id = " + mdIDRefVAL + "\r\n"
                + "-job = verif\r\n"
                + "-job.backend = " + vbBackendRefVAL.toLowerCase() + "\r\n"
                + backendSettingToAdd
                + "-job.req = assertion\r\n"
                + "-job.req.assertion_to_check.0 = " + reqReqtypeAssCheckRefVAL + "\r\n"
                + "-lf = step7\r\n"
                + "-lf.entry = " + entryBlockRefVAL + "\r\n"
                + "-name = \"" + mdNameRefVAL + "\"\r\n"
        }

        // Add the source files
        for(var i = 0; i < caseBuildingCtx.sourceFiles.files.length; i++){
            var fileIndex = caseBuildingCtx.sourceFiles.files[i];
            verification_case = verification_case
                + "-sourcefiles." + i + " = " + workspaceStateCtx.codes[fileIndex].name + "\r\n"
        }
        
        workspaceStateCtx.setCurrentCase(verification_case);
    }

    return (
        <>
            <button data-bs-toggle="modal" data-bs-target="#exampleModal" type="button" className = "btn" onClick = {buildCase} style = {{border: "2px solid black", background: colorThemeCtx.newFileCodeCaseBackground, color: colorThemeCtx.darkModeTextColor, marginTop: "5%"}}>
                Build the Case
            </button>    

            <CaseBuiltPopUp />
        </>
    );
}

export default BuildCaseBtn;