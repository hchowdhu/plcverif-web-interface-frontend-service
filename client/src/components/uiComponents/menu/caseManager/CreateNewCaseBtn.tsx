import { useContext } from "react";
import WorkspaceStateContext from "../../../../store/workspace-state-context";
import ColorThemeContext from "../../../../store/color-theme-context";
import CaseBuildingContext from "../../../../store/caseBuilding-context";

function CreateNewCaseBtn() {

    const workspaceStateCtx = useContext(WorkspaceStateContext);
    const colorThemeCtx = useContext(ColorThemeContext);
    const caseBuildingCtx = useContext(CaseBuildingContext);

    function createNewCase() {
        workspaceStateCtx.setCurrentState([1, 0]);
        caseBuildingCtx.resetCaseBuildingCtx();
        workspaceStateCtx.setCurrentCase("");
    }

    return (
        <button type="button" className = "btn" onClick = {createNewCase} style = {{border: "2px solid black", background: colorThemeCtx.newFileCodeCaseBackground, color: colorThemeCtx.darkModeTextColor}}>
            Create a New Case
        </button>    
    );
}

export default CreateNewCaseBtn;