import FileManager from "./fileManager/FileManager";
import FileManagerNewFileButton from "./fileManager/FileManagerNewFileButton";
import flexClasses from "../../FlexFlowControl.module.css"
import { useContext } from "react";
import WorkspaceStateContext from "../../../store/workspace-state-context";
import ColorThemeContext from "../../../store/color-theme-context";
import CreateNewCaseBtn from "./caseManager/CreateNewCaseBtn";
import BuildCaseBtn from "./caseManager/BuildCaseBtn";
import ReportManagerMain from "./reportManager/ReportManagerMain";
import BuiltInFunctions from "./builtInFunctions/BuiltInFunctions";

function Menu() {

    const workspaceStateCtx = useContext(WorkspaceStateContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    function goToVerificationCase() {
        workspaceStateCtx.setCurrentState([1, workspaceStateCtx.lastCaseWorkspaceState]);
    }

    function goToCodeFiles() {
        workspaceStateCtx.setCurrentState([0, 0]);
    }

    function goToReport() {
        workspaceStateCtx.setLastCaseWorkspaceState(workspaceStateCtx.currentState[1]);
        workspaceStateCtx.setCurrentState([2, 0]);
    }

    return (
        <div style = {{padding: "5%", height: "100%", overflow: "auto"}}>  

            {
                (workspaceStateCtx.currentState[0] == 0) ? 
                    <>
                        <button type="button" className="btn btn-info" style = {{marginBottom: "5%", width: "100%", color: colorThemeCtx.darkModeTextColor}} onClick = {goToVerificationCase}>Go to Verification Case</button>
                        <div className = {flexClasses.fileManagerDivFlexControl} style = {{border: "2px solid black", borderRadius: "1rem", background: colorThemeCtx.fileManagerBackground}}>
                            <h3 style = {{color: colorThemeCtx.darkModeTextColor}}>File Manager</h3>
                            <FileManagerNewFileButton />

                            <div className = {flexClasses.fileManagerFilesFlexControl}>
                                <FileManager />
                            </div>
                        </div>

                        <div className = {flexClasses.builtInFunctionsDivFlexControl} style = {{border: "2px solid black", borderRadius: "1rem", background: colorThemeCtx.fileManagerBackground}}>
                            <h3 style = {{color: colorThemeCtx.darkModeTextColor}}>Built-In Functions</h3>
                        
                            <div className = {flexClasses.builtInFunctionsComponentFlexControl}>
                                <BuiltInFunctions />
                            </div>
                        </div>
                    </>
                : (workspaceStateCtx.currentState[0] == 1) ?
                    <>  
                        <div className="row" style = {{marginBottom: "5%"}}>
                            <div className="col">
                                <button type="button" className="btn btn-info" style = {{width: "100%", color: colorThemeCtx.darkModeTextColor}} onClick = {goToCodeFiles}>Source Files</button>
                            </div>
                            <div className="col">
                                <button type="button" className="btn btn-info" style = {{width: "100%", color: colorThemeCtx.darkModeTextColor}} onClick = {goToReport}>Verify</button>
                            </div>
                        </div>
                        
                        <div className = {flexClasses.caseManagerDivFlexControl} style = {{border: "2px solid black", borderRadius: "1rem", background: colorThemeCtx.fileManagerBackground}}>
                            <h3 style = {{color: colorThemeCtx.darkModeTextColor}}>Case Manager</h3>
                            <CreateNewCaseBtn />

                            {
                                (workspaceStateCtx.currentState[1] == 2) ? 
                                    <BuildCaseBtn />
                                    : null
                            }
                        </div>

                        <div className = {flexClasses.caseManagerDivFlexControl} style = {{marginTop: "5%", border: "2px solid black", borderRadius: "1rem", background: colorThemeCtx.fileManagerBackground}}>
                            <h3 style = {{color: colorThemeCtx.darkModeTextColor}}>Warnings</h3>
                                <ul className="list-group">
                                    {
                                        workspaceStateCtx.caseInputWarnings.map((warning) => (
                                            <li className="list-group-item list-group-item-warning" style = {{marginTop: "2%"}}>
                                                {
                                                    (warning == "output") ?
                                                        <div key = {warning}>You have stated the <code>-output</code> directory. Please remove it.</div>
                                                    : (warning == "sourcefiles_does_not_exist") ?
                                                        <div key = {warning}>No <code>-sourcefiles</code> attribute has been stated. Please include a source file.</div>
                                                    : (warning == "equals") ?
                                                        <div key = {warning}>You have an <code>=</code> sign with no space on the left, or the right, or both.</div>
                                                    : (warning == "sourcefiles_path") ?
                                                        <div key = {warning}>In the <code>-sourcefiles</code> attributes, remove any written path and only put the filename.</div>
                                                    : 
                                                    null
                                                }
                                            </li>
                                        ))
                                    }
                                </ul>
                        </div>
                    </>
                : (workspaceStateCtx.currentState[0] == 2) ?
                    <>
                        <button type="button" className="btn btn-info" style = {{marginBottom: "5%", width: "100%", color: colorThemeCtx.darkModeTextColor}} onClick = {goToVerificationCase}>Go back to Verification Case</button>
                        <ReportManagerMain />
                    </>
                : null
            }
        </div>
    );
}

export default Menu;