import { useContext } from "react";
import WorkspaceStateContext from "../../../../store/workspace-state-context";

interface Props {
    index: number;
}

function FileManagerButtons({index}: Props) {

    const workspaceStateCtx = useContext(WorkspaceStateContext);

    function deleteCorrespondingCode() {
        workspaceStateCtx.deleteCode(index);
    }

    function downloadCodeFile() {
        const blob = new Blob([workspaceStateCtx.codes[index].code], {type: 'text/plain'});
        // Create a URL for the Blob
        const url = URL.createObjectURL(blob);
        // Create a temporary anchor element
        const a = document.createElement('a');
        a.href = url;
        // Specify the filename for the downloaded file
        a.download = workspaceStateCtx.codes[index].name;
        // Trigger a click event on the anchor to start the download
        a.click();
        // Clean up by revoking the Blob URL
        URL.revokeObjectURL(url);
    }

    return (
        <>
            <td>
                <button style = {{border: "none"}} onClick = {downloadCodeFile}>
                    <img src = "download_button.png" style = {{height: "1rem"}}/>
                </button>
            </td>
            <td>
                <button style = {{border: "none"}} onClick = {deleteCorrespondingCode}>
                    <img src = "delete_button.png" style = {{height: "1rem"}}/>
                </button>
            </td>
        </>
    );
}

export default FileManagerButtons;