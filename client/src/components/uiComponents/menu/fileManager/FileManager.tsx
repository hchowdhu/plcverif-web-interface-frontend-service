import isValidFilename from 'valid-filename';
import { useContext, useEffect, useState } from "react";
import WorkspaceStateContext from '../../../../store/workspace-state-context';
import FileManagerButtons from "./FileManagerButtons";
import ColorThemeContext from '../../../../store/color-theme-context';

function FileManager() {

    const workspaceStateCtx = useContext(WorkspaceStateContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    const [fileManagerItems, setFileManagerItems] = useState(
        workspaceStateCtx.codes.map((codeObject) =>
            // Note, the full code like the useEffect is not here because this is not initially seen and we just
            // have to state the type of value which goes here, which is a returned map of the element <tr>
            <tr key = {codeObject.name}></tr>
        )
    );

    // Re-render navbar files everytime the code files array changes
    useEffect(function() {
        setFileManagerItems(
            workspaceStateCtx.codes.map((codeObject, index) =>
                <tr key = {codeObject.name}>
                    <th scope="row">{index + 1}</th>
                    <td>
                        <input 
                            className = "fileManagerInputs" 
                            defaultValue = {codeObject.name} 
                            style = {{width: "100%", border: "none"}}
                            onBlur = {function(e){
                                var modifiedName = e.target.value;
                                if(workspaceStateCtx.nameExistsAlready(modifiedName, workspaceStateCtx.codes)){
                                    // Check if new name is already used
                                    e.target.value = workspaceStateCtx.codes[index].name;
                                } else {
                                    // Change name:
                                    // The RegEx pattern below is for if the the string is, 
                                    // "source", followed by a number, followed by ".scl"
                                    const pattern = /^source\d+\.scl$/;
                                    if(pattern.test(modifiedName)){
                                        e.target.value = workspaceStateCtx.codes[index].name;
                                    } else {
                                        // Check if new name is valid using library:
                                        if(isValidFilename(e.target.value)){
                                            // Check if new name has no spaces
                                            if(!e.target.value.includes(" ")){
                                                // Check/Change if new name has an extension of .scl
                                                if(e.target.value.split(".")[1] != "scl"){
                                                    modifiedName = e.target.value.split(".")[0] + ".scl";
                                                }
                                                // Change the name and update contexts and everything
                                                e.target.value = modifiedName;
                                                workspaceStateCtx.changeNameForCodeFile(index, modifiedName);
                                            } else {
                                                e.target.value = workspaceStateCtx.codes[index].name;
                                            }
                                        } else {
                                            e.target.value = workspaceStateCtx.codes[index].name;
                                        }
                                    }
                                }
                            }}
                        />
                    </td>
                    <FileManagerButtons index = {index}/>
                </tr>
            )
        );
    }, [workspaceStateCtx]); // Basically, if the codes array changes in the context, anyhow

    return (
        <>  
            { // Do not show the blank initial code object. Only show them when a true code object has been added
                (workspaceStateCtx.codes.length != 0) ?
                    (workspaceStateCtx.codes[0].name != "") ? 
                        <table className = "table" style = {{width: "100%"}}>
                            <tbody>
                                {fileManagerItems}
                            </tbody>
                        </table>
                    :
                        <p style = {{color: colorThemeCtx.darkModeTextColor}}>No files created</p>
                : <p style = {{color: colorThemeCtx.darkModeTextColor}}>No files created</p>
            }
        </>
    );
}

export default FileManager;