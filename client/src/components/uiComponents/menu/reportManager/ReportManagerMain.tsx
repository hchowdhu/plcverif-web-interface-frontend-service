import { useContext, useState } from "react";
import WorkspaceStateContext from "../../../../store/workspace-state-context";
import DotGraph from "./DotGraph";
import ColorThemeContext from "../../../../store/color-theme-context";
import CodeMirror from "@uiw/react-codemirror";


function ReportManagerMain() {

    const workspaceStateCtx = useContext(WorkspaceStateContext);
    const colorThemeCtx = useContext(ColorThemeContext);
    
    const [dotName, setDotName] = useState("");
    const [dotValue, setDotValue] = useState("");

    const [fileName, setFileName] = useState("");
    const [fileValue, setFileValue] = useState("");

    function showDOT(index: number) {
        setDotName(workspaceStateCtx.dotFiles[index]);
        setDotValue(workspaceStateCtx.dotFiles[index + 1]);
    }

    function showFile(index: number) {
        setFileName(workspaceStateCtx.allOutputFiles[index]);
        setFileValue(workspaceStateCtx.allOutputFiles[index + 1]);
    }

    function downloadFile() {
        const blob = new Blob([fileValue], {type: 'text/plain'});
        // Create a URL for the Blob
        const url = URL.createObjectURL(blob);
        // Create a temporary anchor element
        const a = document.createElement('a');
        a.href = url;
        // Specify the filename for the downloaded file
        a.download = fileName;
        // Trigger a click event on the anchor to start the download
        a.click();
        // Clean up by revoking the Blob URL
        URL.revokeObjectURL(url);
    }

    return (
        <>
            <div className="accordion" id="accordionExample">
                {
                    workspaceStateCtx.dotFiles.length > 0 ?
                        <div className="accordion-item">
                            <h2 className="accordion-header">
                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                DOT Representations
                            </button>
                            </h2>
                            <div id="collapseOne" className="accordion-collapse collapse" data-bs-parent="#accordionExample">
                                <div className="accordion-body">
                                    <ul className="list-group" style = {{maxHeight: "30vh", overflow: "auto"}}>
                                        {
                                            workspaceStateCtx.dotFiles.map((dotFile, index) =>
                                                <>
                                                    {
                                                        index % 2 == 0 ?
                                                            <li key = {dotFile} className="list-group-item" style = {{padding: "0px", marginTop: "2%", marginBottom: "2%", border: "1px solid gray"}}>
                                                                <button style = {{width: "100%"}} onClick = {function(){showDOT(index)}} type="button" className="btn btn-light" data-bs-toggle="modal" data-bs-target="#exampleModal3">
                                                                    {dotFile}
                                                                </button>
                                                            </li>
                                                        :
                                                        null
                                                    }
                                                </>
                                            )
                                        }
                                    </ul>
                                </div>
                            </div>
                        </div>
                    :
                    null
                }

                {
                    workspaceStateCtx.allOutputFiles.length > 0 ?
                        <div className="accordion-item">
                            <h2 className="accordion-header">
                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Generated Output Files
                                </button>
                            </h2>
                            <div id="collapseTwo" className="accordion-collapse collapse" data-bs-parent="#accordionExample">
                                <div className="accordion-body">
                                    <ul className="list-group" style = {{maxHeight: "30vh", overflow: "auto"}}>
                                        {
                                            workspaceStateCtx.allOutputFiles.map((outputFile, index) =>
                                                <>
                                                    {
                                                        index % 2 == 0 ?
                                                            <li key = {outputFile} className="list-group-item" style = {{padding: "0px", marginTop: "2%", marginBottom: "2%", border: "1px solid gray"}}>
                                                                <button style = {{width: "100%"}} onClick = {function(){showFile(index)}} type="button" className="btn btn-light" data-bs-toggle="modal" data-bs-target="#exampleModal4">
                                                                    {outputFile}
                                                                </button>
                                                            </li>
                                                        :
                                                        null
                                                    }
                                                </>
                                            )
                                        }
                                    </ul>
                                </div>
                            </div>
                        </div>
                    :
                    null
                }
            </div>

            {
                dotValue != "" ?
                    <div className="modal fade" id="exampleModal3" tabIndex = {-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog modal-fullscreen">
                            <div className="modal-content">
                                <div className="modal-header" style = {{color: colorThemeCtx.darkModeTextColor, background: colorThemeCtx.dotModalHeaderBackground}}>
                                    <h1 className="modal-title fs-6" id="exampleModalLabel">{dotName}</h1>
                                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" style = {{filter: "brightness(0) invert(1)"}}></button>
                                </div>
                                <div className="modal-body">
                                    <DotGraph value = {dotValue}/>
                                </div>
                            </div>
                        </div>
                    </div>
                :
                null
            }

            {
                fileValue != "" ?
                    <div className="modal fade" id="exampleModal4" tabIndex = {-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog modal-fullscreen">
                            <div className="modal-content">
                                <div className="modal-header" style = {{color: colorThemeCtx.darkModeTextColor, background: colorThemeCtx.dotModalHeaderBackground}}>
                                    <h1 className="modal-title fs-6" id="exampleModalLabel">{fileName}</h1>
                                    <button type="button" className="btn btn-info" style = {{marginLeft: "1rem"}} onClick = {downloadFile}>Download</button>
                                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" style = {{filter: "brightness(0) invert(1)"}}></button>
                                </div>
                                <div className="modal-body">
                                    <CodeMirror 
                                        value={fileValue} 
                                        height = {"100%"} 
                                        maxHeight = {"100%"}
                                        theme = {colorThemeCtx.codingSpaceTheme}
                                        readOnly = {true}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                :
                null
            }
        </>
    )
}

export default ReportManagerMain;