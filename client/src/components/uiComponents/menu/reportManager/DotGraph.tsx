import Graphviz from "graphviz-react";

interface Props {
    value: string;
}

function DotGraph({value}: Props) {

    return (
        <Graphviz 
            dot = {value}
            options={{
                fit: true,
                height: "100vh",
                width: "100vw",
                zoom: true,
            }}
        />
    )
}

export default DotGraph;