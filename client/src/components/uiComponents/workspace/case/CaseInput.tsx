import { useCallback, useContext, useEffect, useRef, useState } from "react";
import WorkspaceStateContext from "../../../../store/workspace-state-context";
import flexClasses from '../../../FlexFlowControl.module.css'
import CodeMirror from "@uiw/react-codemirror";
import ColorThemeContext from "../../../../store/color-theme-context";

function CaseInput() {

    const workspaceStateCtx = useContext(WorkspaceStateContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    const caseInputContainerRef: any = useRef(null);

    // This function is called when the component loads to set the max height in pixels to
    // the height corresponding to "100%". 
    function returnMaxAllowedHeight() {
        return (caseInputContainerRef.current == null) ? "" : (caseInputContainerRef.current["clientHeight"] + "px");
    }

    const [maxHeight, setMaxHeight] = useState(returnMaxAllowedHeight());

    useEffect(function() {
        setMaxHeight(returnMaxAllowedHeight());
    }, [window.innerHeight, window.innerWidth]);

    useEffect(function() {
        createWarnings(workspaceStateCtx.case);
    }, [])

    function createWarnings(value: string) {
        // Check case syntax, warnings etc here
        var warnings = [];
        // Check if output attribute exists (which it should not)
        if(value.includes("\n-output")) {
            warnings.push("output");
        }
        // Check if sourcefile attribute exists (which it should)
        if(!value.includes("\n-sourcefiles.")) {
            warnings.push("sourcefiles_does_not_exist");
        }
        // Check if any '=' sign has no space after
        var valueSplitByEqualSigns = value.split("=");
        var foundErrorWithEqualSign = false;
        // start iteration from 1 to avoid undefined "i - 1" when i = 0. Also end iteration at 1 less for undefined "i + 1" at last element.
        for(var i = 1; i < valueSplitByEqualSigns.length - 1; i++) {
            if(valueSplitByEqualSigns[i].includes("\n-")) {
                if(
                    valueSplitByEqualSigns[i][valueSplitByEqualSigns[i].length - 1] != " " || 
                    valueSplitByEqualSigns[i + 1][0] != " "
                ) {
                    foundErrorWithEqualSign = true;
                    break;
                }
            }
        }
        if(foundErrorWithEqualSign) {
            warnings.push("equals")
        }
        // Check if sourcefile tags have a path
        var valueSplitByEqualSigns = value.split("=");
        var sourceFileHasPath = false;
        for(var i = 1; i < valueSplitByEqualSigns.length; i++) { // start iteration from 1 to avoid undefined "i - 1" when i = 0
            if(valueSplitByEqualSigns[i - 1].includes("\n-sourcefiles.")) {
                if(valueSplitByEqualSigns[i].includes("/") || valueSplitByEqualSigns[i].includes("\\")) {
                    sourceFileHasPath = true;
                }
            }
        }
        if(sourceFileHasPath) {
            warnings.push("sourcefiles_path")
        }
        // Set the warnings
        workspaceStateCtx.setCaseInputWarnings(warnings);
    }

    const updateContextCase = useCallback((value: string) => {
        workspaceStateCtx.setCurrentCase(value);

        createWarnings(value);
    }, [workspaceStateCtx]);

    return (
        <>
            <div className = {flexClasses.caseInputFlexControl} ref = {caseInputContainerRef}>
                <CodeMirror 
                    value={workspaceStateCtx.case} 
                    height = {"100%"} 
                    maxHeight = {maxHeight}
                    className = {flexClasses.caseInputTextAreaFlexControl}
                    onChange = {updateContextCase}
                    theme = {colorThemeCtx.codingSpaceTheme}
                />
            </div>
        </>
    );
}

export default CaseInput;