import { useContext } from "react";
import Metadata from "./Metadata";
import Requirement from "./Requirement";
import SourceFiles from "./SourceFiles";
import VerifBackend from "./VerifBackend";
import ColorThemeContext from "../../../../../store/color-theme-context";

function CaseBuild() {

    const colorThemeCtx = useContext(ColorThemeContext);

    return (
        <div id="carouselExample" className="carousel slide" style = {{width: "100%", height: "100%"}} data-bs-interval="false">
            <div className="carousel-inner" style = {{height: "100%"}}>
                <div className="carousel-item active" style = {{height: "100%"}}>
                    <Metadata />
                </div>
                <div className="carousel-item" style = {{height: "100%"}}>
                    <SourceFiles />
                </div>
                <div className="carousel-item" style = {{height: "100%"}}>
                    <VerifBackend />
                </div>
                <div className="carousel-item" style = {{height: "100%"}}>
                    <Requirement />
                </div>
            </div>

            <button className="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev"
                style = {{height: "100%", background: colorThemeCtx.caseBuildCarouselArrowsBackground, width: "7.5%", border: "2px solid white"}}
            >
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Previous</span>
            </button>
            <button className="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next"
                style = {{height: "100%", background: colorThemeCtx.caseBuildCarouselArrowsBackground, width: "7.5%", border: "2px solid white"}}
            >
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Next</span>
            </button>
        </div>
    );
}

export default CaseBuild;

