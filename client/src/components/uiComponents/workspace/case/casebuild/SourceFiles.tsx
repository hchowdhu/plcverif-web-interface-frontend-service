import flexClasses from "../../../../FlexFlowControl.module.css"
import { useContext, useRef, useState } from "react";
import WorkspaceStateContext from "../../../../../store/workspace-state-context";
import CaseBuildingContext from "../../../../../store/caseBuilding-context";
import ColorThemeContext from "../../../../../store/color-theme-context";

function SourceFiles() {

    const workspaceStateCtx = useContext(WorkspaceStateContext);
    const caseBuildingCtx = useContext(CaseBuildingContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    const [selectedSourceFiles, setSelectedSourceFiles] = useState<number[]>([]);
    const [entryBlock, setEntryBlock] = useState("");
    const [customEntryBlock, setCustomEntryBlock] = useState("");
    const [detectedOrCustom, setDetectedOrCustom] = useState(true); // false is custom, true is detected

    const entryBlockRef = useRef(null);

    function identifyEntryBlock() {
        if(detectedOrCustom) {
            var pattern1 = /FUNCTION_BLOCK (\w+)/;
            var pattern2 = /FUNCTION (\w+)/;
            var foundMatch = false;
            for(var i = 0; i < selectedSourceFiles.length; i++) {
                var code = workspaceStateCtx.codes[selectedSourceFiles[i]].code;
                var match1 = pattern1.exec(code);
                var match2 = pattern2.exec(code);
                if(match1) {
                    foundMatch = true;
                    setEntryBlock(match1[1]); // matched string will be second element of the array
                } else if(match2) {
                    foundMatch = true;
                    setEntryBlock(match2[1]); // matched string will be second element of the array
                }
            }
            if(foundMatch == false) {
                setEntryBlock("");
            }
        }
    }

    function saveSourceFilesInfo() {
        const entryBlockRefVAL = entryBlockRef.current == null ? "" : entryBlockRef.current["value"];
        if(!detectedOrCustom) {
            // If the user wants to put a custom entry block, then update the value of the input field to the entryBlock useState
            const entryBlockRefVAL = entryBlockRef.current == null ? "" : entryBlockRef.current["value"];
            setCustomEntryBlock(entryBlockRefVAL);
        }

        identifyEntryBlock();

        caseBuildingCtx.setSourceFiles({
            files: selectedSourceFiles,
            entryBlock: detectedOrCustom ? entryBlock : entryBlockRefVAL
        });
    }

    return (
        <div className="card text-center" style = {{height: "100%", width: "85%", left: "7.5%", background: colorThemeCtx.caseBuildContentBackground}} onChange = {saveSourceFilesInfo}>
            <div className="card-header" style = {{background: colorThemeCtx.caseBuildHeaderBackground, color: colorThemeCtx.darkModeTextColor}}>
                <h5 style = {{marginTop: "1%"}}>2. Source Files</h5>
            </div>
            <div className = {["card-body", flexClasses.caseBuildContentFlexControl].join(" ")} style = {{height: "100%", color: colorThemeCtx.darkModeTextColor}}>
                <h5 className="card-title"><u>Source File(s) Selection</u></h5>
                <p className="card-text">Select which source files you would like to include in the verification.</p>
                    {
                        (workspaceStateCtx.codes.length != 0) ? 
                            (workspaceStateCtx.codes[0].name != "") ?
                                <div style = {{height: "20%", marginTop: "3%", overflowY: "auto", border: "1px solid black", background: "white"}}>
                                    {
                                        workspaceStateCtx.codes.map((codeObject, index) =>
                                            <div key = {index}>
                                                <input type="checkbox" className="btn-check" id = {"btn-check-" + index} autoComplete="off"/>
                                                <label className="btn btn-outline-primary" htmlFor = {"btn-check-" + index} style = {{width: "100%"}} onClick = {
                                                    function(){
                                                        let modifiedSelection = selectedSourceFiles;
                                                        if(modifiedSelection.includes(index)){
                                                            modifiedSelection.splice(modifiedSelection.indexOf(index), 1);
                                                        } else {
                                                            modifiedSelection.push(index)
                                                        }
                                                        setSelectedSourceFiles(modifiedSelection);
                                                        identifyEntryBlock();
                                                    }
                                                }>{codeObject.name}</label>
                                            </div>
                                        )
                                    }
                                </div>
                            : "(No files created yet)"
                        : "(No files created yet)"
                    }
                
                <h5 className="card-title" style = {{marginTop: "10%"}}><u>Entry Block</u></h5>
                
                <div className="container text-center" style = {{marginTop: "2%"}}>
                    <div className="row">
                        <div className="col-2">
                            <input type="checkbox" className="btn-check" name="options-base" id="option5" autoComplete="off" checked = {detectedOrCustom} onClick = {function(){setDetectedOrCustom(true)}}/>
                            <label className="btn btn-outline-info" htmlFor="option5" style = {{color: colorThemeCtx.darkModeTextColor}}>Detected</label>
                        </div>
                        <div className="col-2">
                            <input type="checkbox" className="btn-check" name="options-base" id="option6" autoComplete="off" checked = {!detectedOrCustom} onClick = {function(){setDetectedOrCustom(false)}}/>
                            <label className="btn btn-outline-info" htmlFor="option6" style = {{color: colorThemeCtx.darkModeTextColor}}>Custom</label>
                        </div>
                        <div className="col-8">
                            <input 
                                style = {{border: detectedOrCustom ? "" : "1px solid black"}} 
                                type="text" className="form-control" 
                                aria-label="Sizing example input" 
                                aria-describedby="inputGroup-sizing-default" 
                                readOnly = {detectedOrCustom} 
                                ref = {entryBlockRef}
                                value = {detectedOrCustom ? entryBlock : customEntryBlock} 
                                placeholder={detectedOrCustom ? "Entry Block not detected" : "No Entry Block defined"}
                            />
                        </div>
                    </div>
                </div>
                
                <p className="card-text"></p>
            </div>
            <div className="card-footer text-body-secondary" style = {{background: colorThemeCtx.caseBuildFooterBackground}}>
                <div style = {{color: colorThemeCtx.darkModeTextColor}}>Go through all four steps. When you're done, build the case and then verify!</div>
            </div>
        </div>
    );
}

export default SourceFiles;


