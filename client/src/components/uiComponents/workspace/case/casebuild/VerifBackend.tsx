import flexClasses from "../../../../FlexFlowControl.module.css"
import CaseBuildingContext from "../../../../../store/caseBuilding-context";
import { useContext, useRef, useState } from "react";
import ColorThemeContext from "../../../../../store/color-theme-context";

function VerifBackend() {

    const caseBuildingCtx = useContext(CaseBuildingContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    const [selectedBackend, setSelectedBackend] = useState(caseBuildingCtx.verificationBackend.backend == "" ? "NuSMV" : caseBuildingCtx.verificationBackend.backend);

    const vbBackendRef = useRef(null);
    const vbBackendNuSMVAlgorithmRef = useRef(null);
    const vbBackendCBMCESBMCModVarRef = useRef(null);

    function updateSelectedBackend(event: any) {
        setSelectedBackend(event.target.value);
    }

    function saveVerifBackendInfo() {
        const vbBackendRefVAL = vbBackendRef.current == null ? "" : vbBackendRef.current["value"];
        const vbBackendNuSMVAlgorithmRefVAL = vbBackendNuSMVAlgorithmRef.current == null ? "" : vbBackendNuSMVAlgorithmRef.current["value"];
        const vbBackendCBMCESBMCModVarRefVAL = vbBackendCBMCESBMCModVarRef.current == null ? "" : vbBackendCBMCESBMCModVarRef.current["value"];
        
        if(vbBackendRefVAL.toString() == "NuSMV"){
            caseBuildingCtx.setVerificationBackend({
                backend: vbBackendRefVAL,
                algorithm: vbBackendNuSMVAlgorithmRefVAL
            });
        } else if(vbBackendRefVAL.toString() == "CBMC" || vbBackendRefVAL.toString() == "ESBMC") {
            caseBuildingCtx.setVerificationBackend({
                backend: vbBackendRefVAL,
                modelVariant: vbBackendCBMCESBMCModVarRefVAL,
            });
        }
    }

    return (
        <div className="card text-center" style = {{height: "100%", width: "85%", left: "7.5%", background: colorThemeCtx.caseBuildContentBackground, color: colorThemeCtx.darkModeTextColor}} onChange = {saveVerifBackendInfo}>
            <div className="card-header" style = {{background: colorThemeCtx.caseBuildHeaderBackground}}>
                <h5 style = {{marginTop: "1%"}}>3. Verification Backend</h5>
            </div>
            <div className = {["card-body", flexClasses.caseBuildContentFlexControl].join(" ")}>
                <label className="form-label">Backend:</label>
                <select defaultValue = {caseBuildingCtx.verificationBackend.backend} className="form-select" aria-label="Default select example" onChange = {updateSelectedBackend} ref = {vbBackendRef}>
                    <option value="NuSMV" selected>NuSMV</option>
                    <option value="CBMC">CBMC</option>
                    <option value="ESBMC">ESBMC</option>
                </select>
                <br></br>
                {
                    selectedBackend == "NuSMV" ? 
                        <div>
                            <label className="form-label">Algorithm:</label>
                            <select defaultValue = {caseBuildingCtx.verificationBackend.algorithm} className="form-select form-select-sm" aria-label="Small select example" ref = {vbBackendNuSMVAlgorithmRef}>
                                <option value="Default" selected>Default</option>
                                <option value="Classic">Classic</option>
                                <option value="Ic3">Ic3</option>
                            </select>
                        </div>
                        : (selectedBackend == "CBMC" || selectedBackend == "ESBMC") ?
                            <div>
                                <label className="form-label">Model Variant:</label>
                                <select defaultValue = {caseBuildingCtx.verificationBackend.modelVariant} className="form-select form-select-sm" aria-label="Small select example" ref = {vbBackendCBMCESBMCModVarRef}>
                                    <option value="Default" selected>Default</option>
                                    <option value="CFA declaration">CFA declaration</option>
                                    <option value="CFA instance">CFA instance</option>
                                    <option value="Structured CFA declaration">Structured CFA declaration</option>
                                </select>
                            </div>
                            : null
                }
            </div>
            <div className="card-footer text-body-secondary" style = {{background: colorThemeCtx.caseBuildFooterBackground}}>
                <div style = {{color: colorThemeCtx.darkModeTextColor}}>Go through all four steps. When you're done, build the case and then verify!</div>
            </div>
        </div>
    );
}

export default VerifBackend;


