import CodeStart from "./code/CodeStart";
import CodeInput from "./code/CodeInput";
import WorkspaceStateContext from "../../../store/workspace-state-context";
import { useContext } from "react";
import CodeNavbar from "./code/CodeNavbar";
import flexClasses from '../../FlexFlowControl.module.css'
import CaseStart from "./case/CaseStart";
import CaseInput from "./case/CaseInput";
import CaseBuild from "./case/casebuild/CaseBuild";
import Report from "./report/Report";

function Workspace() {
    const workspaceStateCtx = useContext(WorkspaceStateContext);

    return (
        <div style = {{height: "100%"}}>
            {(workspaceStateCtx.currentState[0] == 0) ? 
                <CodeNavbar />
                : null
            }

            {
                (workspaceStateCtx.currentState[0] == 0 && workspaceStateCtx.currentState[1] == 0) ?
                    <div className = {flexClasses.codeWritingWorkspaceFlexControl}><CodeStart /></div>
                    : (workspaceStateCtx.currentState[0] == 0 && workspaceStateCtx.currentState[1] == 1) ?
                        <div className = {flexClasses.codeWritingWorkspaceFlexControl}><CodeInput /></div>
                        : (workspaceStateCtx.currentState[0] == 1 && workspaceStateCtx.currentState[1] == 0) ?
                            <div className = {flexClasses.caseWritingWorkspaceFlexControl}><CaseStart /></div>
                            : (workspaceStateCtx.currentState[0] == 1 && workspaceStateCtx.currentState[1] == 1) ?
                                <div className = {flexClasses.caseWritingWorkspaceFlexControl}><CaseInput /></div>
                                : (workspaceStateCtx.currentState[0] == 1 && workspaceStateCtx.currentState[1] == 2) ?
                                    <div style = {{height: "100%"}}><CaseBuild /></div>
                                    : (workspaceStateCtx.currentState[0] == 2 && workspaceStateCtx.currentState[1] == 0) ?
                                        <div style = {{height: "100%"}}><Report /></div>
                                        : null
            }
        </div>
    );
}

export default Workspace;