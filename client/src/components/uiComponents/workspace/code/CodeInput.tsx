import { useCallback, useContext, useEffect, useRef, useState } from "react";
import WorkspaceStateContext from "../../../../store/workspace-state-context";
import flexClasses from '../../../FlexFlowControl.module.css'
import CodeMirror from "@uiw/react-codemirror"
import ColorThemeContext from "../../../../store/color-theme-context";

function CodeInput() {

    const workspaceStateCtx = useContext(WorkspaceStateContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    const codeInputContainerRef: any = useRef(null);

    // This function is called when the component loads to set the max height in pixels to
    // the height corresponding to "100%". 
    function returnMaxAllowedHeight() {
        return (codeInputContainerRef.current == null) ? "" : (codeInputContainerRef.current["clientHeight"] + "px");
    }

    const [maxHeight, setMaxHeight] = useState(returnMaxAllowedHeight());

    useEffect(function() {
        setMaxHeight(returnMaxAllowedHeight());
    }, [window.innerHeight, window.innerWidth]);

    const updateContextCodeForCurrentFile = useCallback((value: string) => {
        workspaceStateCtx.setCodeForCodeFile(workspaceStateCtx.selectedCodeWorkspaceFile, value);
    }, [workspaceStateCtx]);

    return (
        <>
            <div className = {flexClasses.codeInputFlexControl} ref = {codeInputContainerRef}>
                <CodeMirror 
                    value={workspaceStateCtx.codes[workspaceStateCtx.selectedCodeWorkspaceFile].code} 
                    height = {"100%"} 
                    maxHeight = {maxHeight}
                    className = {flexClasses.codeInputTextAreaFlexControl}
                    onChange = {updateContextCodeForCurrentFile}
                    theme = {colorThemeCtx.codingSpaceTheme}
                />
            </div>
        </>
    );
}

export default CodeInput;