
import { useContext, useEffect, useState } from "react";
import WorkspaceStateContext from "../../../../store/workspace-state-context";
import classes from "./CodeNavbar.module.css"
import ColorThemeContext from "../../../../store/color-theme-context";

function CodeNavbar() {

    const workspaceStateCtx = useContext(WorkspaceStateContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    function updateCodeWorkspaceFile(fileNum: number){
        workspaceStateCtx.setSelectedCodeWorkspaceFile(fileNum);
    }

    const [navBarItems, setNavBarItems] = useState(
        workspaceStateCtx.codes.map((codeObject, index) =>
            // Doing this conditional statement to avoid rendering the first blank, initial, "file" from the context's intitial value
            (codeObject.name != "") ?
                <button style = {{borderBottom: (workspaceStateCtx.selectedCodeWorkspaceFile == index ? "none" : "2px solid black"), background: (workspaceStateCtx.selectedCodeWorkspaceFile == index ? colorThemeCtx.fileTabsSelectedBackground : colorThemeCtx.fileTabsUnSelectedBackground), color: colorThemeCtx.darkModeTextColor}} className = {["col-2", classes.navBarButtonFiles].join(" ")} key = {codeObject.name} onClick = {function(){updateCodeWorkspaceFile(index)}}>
                    {codeObject.name}
                </button>
            :
            null
        )
    );

    // Re-render navbar files everytime the code files array changes
    useEffect(function() {
        setNavBarItems(
            workspaceStateCtx.codes.map((codeObject, index) =>
                // Doing this conditional statement to avoid rendering the first blank, initial, "file" from the context's intitial value
                (codeObject.name != "") ?
                    <button style = {{borderBottom: (workspaceStateCtx.selectedCodeWorkspaceFile == index ? "none" : "2px solid black"), background: (workspaceStateCtx.selectedCodeWorkspaceFile == index ? colorThemeCtx.fileTabsSelectedBackground : colorThemeCtx.fileTabsUnSelectedBackground), color: colorThemeCtx.darkModeTextColor}} className = {["col-2", classes.navBarButtonFiles].join(" ")} key = {codeObject.name} onClick = {function(){updateCodeWorkspaceFile(index)}}>
                        {codeObject.name}
                    </button>
                :
                null
            )
        );
    }, [workspaceStateCtx]); // Basically, if the codes array changes in the context or if the selectedCodeWorkspace changes, anyhow

    return (
        // Note that the height of the NavBar is 6% which means that the height of the textarea is 94%
        // So, if you want to change one, you must change the other so that the total is 100%. Also,
        // you can change the height of the textarea in the .codeWritingWorkspaceFlexControl class of the
        // FlexFlowControl.module.css file.
        <div style = {{height: "6%", background: colorThemeCtx.navBarCodeBackground}}>
            <div style = {{height: "100%", overflow: "auto", overflowY: "hidden", whiteSpace: "nowrap"}}>
                {navBarItems}
            </div>
        </div>
    );
}   

export default CodeNavbar;