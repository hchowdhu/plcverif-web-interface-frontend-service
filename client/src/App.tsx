import './App.css'
import Header from './components/pageFeatures/Header';
import PageBody from './components/pageFeatures/PageBody';
import 'bootstrap/dist/css/bootstrap.css'
import flexClasses from './components/FlexFlowControl.module.css'
import { useContext } from 'react';
import ColorThemeContext from './store/color-theme-context';

function App() {
	
	var colorThemeCtx = useContext(ColorThemeContext);

  	return (
		<div className = {flexClasses.appFlexControl} style = {{background: colorThemeCtx.pageBackground}}>
			<Header />
			<PageBody />
		</div>
  	);
}

export default App;
