import { ReactNode, createContext, useState } from "react";

/*
    CurrentState Explanation:
    0, 0 --> code, start
    0, 1 --> code, input
    1, 0 --> case, start
    1, 1 --> case, input
    1, 2 --> case, build
    2, 0 --> report
*/
const WorkspaceStateContext = createContext({
    currentState: [0, 0],
    codes: [{name: "", code: ""}],
    case: "",
    selectedCodeWorkspaceFile: 0,
    lastCaseWorkspaceState: 0,
    dotFiles: [],
    allOutputFiles: [],
    listOfBuiltInFunctions: [],
    caseInputWarnings: [],
    setCurrentState: (_state: any) => {},
    setCurrentCodes: (_code: any) => {},
    setCurrentCase: (_case: string) => {},
    addCode: (_name: string, _code: string) => {},
    setSelectedCodeWorkspaceFile: (_fileNum: number) => {},
    setCodeForCodeFile: (_fileNum: number, _code: string) => {},
    changeNameForCodeFile: (_fileNum: number, _name: string) => {},
    deleteCode: (_fileNum: number) => {},
    refresh: () => {},
    nameExistsAlready: (_name: string, _array: any): boolean => false,
    setLastCaseWorkspaceState: (_state: number) => {},
    setDotFiles: (_files: any) => {},
    setAllOutputFiles: (_files: any) => {},
    setListOfBuiltInFunctions: (_content: string) => {},
    setCaseInputWarnings: (_warnings: any) => {}
});

interface Props {
    children: ReactNode;
}

export function WorkspaceStateContextProvider({children} : Props){

    const [currentWorkspaceState, setCurrentWorkspaceState] = useState([0, 0]);
    const [currentWorkspaceCodes, setCurrentWorkspaceCodes] = useState([{name: "", code: ""}]);
    const [currentWorkspaceCase, setCurrentWorkspaceCase] = useState("");
    const [currentSelectedCodeWorkspaceFile, setCurrentSelectedCodeWorkspaceFile] = useState(0);
    const [refresher, setRefresher] = useState("");
    const [blankFileSuffixNumber, setBlankFileSuffixNumber] = useState(2);
    const [currentLastCaseWorkspaceState, setCurrentLastCaseWorkspaceState] = useState(0);
    const [currentDotFiles, setCurrentDotFiles] = useState([]);
    const [currentAllOutputFiles, setCurrentAllOutputFiles] = useState([]);
    const [currentListOfBuiltInFunctions, setCurrentListOfBuiltInFunctions] = useState([]);
    const [currentCaseInputWarnings, setCurrentCaseInputWarnings] = useState([]);
    function updateWorkspaceState(state: any) {
        setCurrentWorkspaceState(state);
        refreshComponents();
    }

    function updateWorkspaceCodes(codes: any) {
        setCurrentWorkspaceCodes(codes);
        refreshComponents();
    }

    function updateWorkspaceCase(_case: string) { // case is inbuilt. So, used _case instead
        setCurrentWorkspaceCase(_case);
        refreshComponents();
    }

    function addNewWorkspaceCode(name: string, code: string) {
        if(name == ""){
            // If a blank file is created, increase the number of blank files created variable
            setBlankFileSuffixNumber(blankFileSuffixNumber + 1);
        }
        name = (name == "") ? ("source" + blankFileSuffixNumber + ".scl") : name;
        let updatedArray = [...currentWorkspaceCodes, {name: name, code: code}];
        setCurrentWorkspaceCodes(updatedArray);

        // Make selected workspace file be the new created file
        setCurrentSelectedCodeWorkspaceFile(currentWorkspaceCodes.length);

        refreshComponents();
    }

    function updateSelectedCodeWorkspaceFile(fileNum: number) {
        setCurrentSelectedCodeWorkspaceFile(fileNum);
        refreshComponents();
    }

    function updateCodeForCodeFile(fileNum: number, code: string) {
        var modifiedWorkspaceCodes = currentWorkspaceCodes;
        modifiedWorkspaceCodes[fileNum].code = code;
        setCurrentWorkspaceCodes(modifiedWorkspaceCodes);
        refreshComponents();
    }

    function updateNameForCodeFile(fileNum: number, name: string){
        var modifiedWorkspaceCodes = currentWorkspaceCodes;
        modifiedWorkspaceCodes[fileNum].name = name;
        setCurrentWorkspaceCodes(modifiedWorkspaceCodes);
        refreshComponents();
    }

    function deleteWorkspaceCode(fileNum: number) {
        // If you're viewing the file which is to be deleted
        if(fileNum == currentSelectedCodeWorkspaceFile){
            // If it the file has an index of 0 (e.g the first file)
            if(fileNum == 0){
                // If the length of the codes array is 1 now, it means
                // that after the deletion, there will be no files at all
                // So, in this case, return to the start screen state
                if(currentWorkspaceCodes.length == 1){
                    setCurrentWorkspaceState([0, 0]);
                }
            } else if(fileNum == currentWorkspaceCodes.length - 1){
                // Else if it the file at the end of the array (e.g the last file)
                // Then, set the currentSelectedCodeWorkspaceFile to be 1 less
                setCurrentSelectedCodeWorkspaceFile(currentSelectedCodeWorkspaceFile - 1);
            }
        } else if(currentSelectedCodeWorkspaceFile == currentWorkspaceCodes.length - 1){
            // If you're viewing the last file and you deleted a file of a lower index than it
            // Almost same logic and reasoning as last else if statement of above if statement
            setCurrentSelectedCodeWorkspaceFile(currentSelectedCodeWorkspaceFile - 1);
        }

        // Delete the file
        var modifiedWorkspaceCodes = currentWorkspaceCodes;
        modifiedWorkspaceCodes.splice(fileNum, 1);
        setCurrentWorkspaceCodes(modifiedWorkspaceCodes);
        refreshComponents();
        console.log(fileNum, currentSelectedCodeWorkspaceFile);
    }

    function refreshComponents() {
        setRefresher(refresher + ".");
    }

    function checkIfNameAlreadyExists(name: string, array: any) {
        for(var i = 0; i < array.length; i++){
            if(array[i].name == name){
                return true;
            }
        }
        return false;
    }

    function updateLastCaseWorkspaceState(state: number) {
        setCurrentLastCaseWorkspaceState(state);
    }

    function updateDotFiles(files: any) {
        setCurrentDotFiles(files);
    }

    function updateAllOutputFiles(files: any) {
        setCurrentAllOutputFiles(files);
    }

    function updateListOfBuiltInFunctions(content: string) {
        var list: any = [];

        // Identify all words after "FUNCTION " amd "FUNCTION_BLOCK" by splitting up the content by " "
        var contentSplitBySpaces = content.split(" ");
        for(var i = 0; i < contentSplitBySpaces.length; i++) {
            if(contentSplitBySpaces[i - 1] == "FUNCTION" || contentSplitBySpaces[i - 1] == "FUNCTION_BLOCK") {
                list.push((contentSplitBySpaces[i]));
            } 
        }

        setCurrentListOfBuiltInFunctions(list);
    }

    function updateCaseInputWarnings(warnings: any) {
        setCurrentCaseInputWarnings(warnings);
    }

    const context = {
        currentState: currentWorkspaceState,
        codes: currentWorkspaceCodes,
        case: currentWorkspaceCase,
        selectedCodeWorkspaceFile: currentSelectedCodeWorkspaceFile,
        lastCaseWorkspaceState: currentLastCaseWorkspaceState,
        dotFiles: currentDotFiles,
        allOutputFiles: currentAllOutputFiles,
        listOfBuiltInFunctions: currentListOfBuiltInFunctions,
        caseInputWarnings: currentCaseInputWarnings,
        setCurrentState: updateWorkspaceState,
        setCurrentCodes: updateWorkspaceCodes,
        setCurrentCase: updateWorkspaceCase,
        addCode: addNewWorkspaceCode,
        setSelectedCodeWorkspaceFile: updateSelectedCodeWorkspaceFile,
        setCodeForCodeFile: updateCodeForCodeFile,
        changeNameForCodeFile: updateNameForCodeFile,
        deleteCode: deleteWorkspaceCode,
        refresh: refreshComponents,
        nameExistsAlready: checkIfNameAlreadyExists,
        setLastCaseWorkspaceState: updateLastCaseWorkspaceState,
        setDotFiles: updateDotFiles,
        setAllOutputFiles: updateAllOutputFiles,
        setListOfBuiltInFunctions: updateListOfBuiltInFunctions,
        setCaseInputWarnings: updateCaseInputWarnings
    };

    return (
        <WorkspaceStateContext.Provider value = {context}>
            {children}
        </WorkspaceStateContext.Provider>
    );
}

export default WorkspaceStateContext;
