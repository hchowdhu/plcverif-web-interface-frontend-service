import { ReactNode, createContext, useState } from "react";

const CaseBuildingContext = createContext({
    metadata: {
        id: "",
        name: "",
        description: ""
    },
    sourceFiles: {
        files: [],
        entryBlock: ""
    },
    verificationBackend: {
        backend: "",
        algorithm: "",
        modelVariant: "",
    },
    requirement: {
        type: "",
        check: "",
        pattern: "",
        one: "",
        two: "",
        three: ""
    },
    setMetaData: (_metadata: any) => {},
    setSourceFiles: (_sourceFiles: any) => {},
    setVerificationBackend: (_verificationBackend: any) => {},
    setRequirement: (_requirement: any) => {},
    resetCaseBuildingCtx: () => {}
});

interface Props {
    children: ReactNode;
}

export function CaseBuildingContextProvider({children} : Props){

    const [currentMetaData, setNewMetaData] = useState({
        id: "",
        name: "",
        description: ""
    });
    const [currentSourceFiles, setNewSourceFiles] = useState({
        files: [],
        entryBlock: ""
    });
    const [currentVerificationBackend, setNewVerificationBackend] = useState({
        backend: "NuSMV",
        algorithm: "Default",
        modelVariant: "Default",
    });
    const [currentRequirement, setNewRequirement] = useState({
        type: "Assertion",
        check: "",
        pattern: "",
        one: "",
        two: "",
        three: ""
    });

    function setNewCurrentMetaData(newMetaData: any) {
        setNewMetaData(newMetaData);
    }

    function setNewCurrentSourceFiles(newSourceFiles: any) {
        setNewSourceFiles(newSourceFiles);
    }

    function setNewCurrentVerificationBackend(newVerificationBackend: any) {
        setNewVerificationBackend(newVerificationBackend);
    }

    function setNewCurrentRequirement(newRequirement: any) {
        setNewRequirement(newRequirement);
    }

    function resetCaseBuildingContext() {
        setNewMetaData({
            id: "",
            name: "",
            description: ""
        });
        setNewSourceFiles({
            files: [],
            entryBlock: ""
        });
        setNewVerificationBackend({
            backend: "NuSMV",
            algorithm: "Default",
            modelVariant: "Default",
        });
        setNewRequirement({
            type: "Assertion",
            check: "",
            pattern: "",
            one: "",
            two: "",
            three: ""
        });
    }
    
    const context = {
        metadata: currentMetaData,
        sourceFiles: currentSourceFiles,
        verificationBackend: currentVerificationBackend,
        requirement: currentRequirement,
        setMetaData: setNewCurrentMetaData,
        setSourceFiles: setNewCurrentSourceFiles,
        setVerificationBackend: setNewCurrentVerificationBackend,
        setRequirement: setNewCurrentRequirement,
        resetCaseBuildingCtx: resetCaseBuildingContext
    };

    return (
        <CaseBuildingContext.Provider value = {context}>
            {children}
        </CaseBuildingContext.Provider>
    );
}

export default CaseBuildingContext;