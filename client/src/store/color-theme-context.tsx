
import { ReactNode, createContext, useState } from "react";
import { solarizedDark } from '@uiw/codemirror-theme-solarized'

const themes = [
    {
      "name": "Dark Mode (Shades of Blue)",
      "colors": ["#001F3F", "#003366", "#004C99", "#0066CC", "#0077FF", "#3399FF", "#66B2FF", "#012B36"]
    },
    {
      "name": "Light Mode (Eclipse-style)",
      "colors": ["#959595", "#A4A4A4", "#B3B3B3", "#C2C2C2", "#D1D1D1", "#E0E0E0", "#F0F0F0", "#FFFFFF"]
    }
];

const ColorThemeContext = createContext({
    codingSpaceTheme: solarizedDark,
    pageBackground: "",
    headerBackground: "",
    menuBackground: "",
    startBackground: "",
    navBarCodeBackground: "",
    fileManagerBackground: "", // also used for caseManager
    fileTabsSelectedBackground: "",
    fileTabsUnSelectedBackground: "",
    newFileCodeCaseBackground: "",
    caseBuildCarouselArrowsBackground: "",
    caseBuildHeaderBackground: "",
    caseBuildContentBackground: "", 
    caseBuildFooterBackground: "",
    caseCodeStartButtonsBackground: "",
    darkModeTextColor: "",
    dotModalHeaderBackground: ""
});

interface Props {
    children: ReactNode;
}

export function ColorThemeContextProvider({children} : Props){

    const [theme] = useState(0);
    var themeColors = themes[theme].colors;

    const [codingSpaceTheme] = useState(solarizedDark);
    const [pageBackground] = useState(themeColors[0]);
    const [headerBackground] = useState(themeColors[1]);
    const [menuBackground] = useState(themeColors[2]);
    const [startBackground] = useState(themeColors[4]);
    const [navBarCodeBackground] = useState(themeColors[3]);
    const [fileManagerBackground] = useState(themeColors[4]); // also used for other menu sections
    const [fileTabsSelectedBackground] = useState(themeColors[0]);
    const [fileTabsUnSelectedBackground] = useState(themeColors[5]);
    const [newFileCodeCaseBackground] = useState(themeColors[5]);
    const [caseBuildCarouselArrowsBackground] = useState(themeColors[3]);
    const [caseBuildHeaderBackground] = useState(themeColors[3]);
    const [caseBuildContentBackground] = useState(themeColors[5]);
    const [caseBuildFooterBackground] = useState(themeColors[3]);
    const [caseCodeStartButtonsBackground] = useState(themeColors[6]);
    const [darkModeTextColor] = useState("white");
    const [dotModalHeaderBackground] = useState(themeColors[3]);

    const context = {
        codingSpaceTheme: codingSpaceTheme,
        pageBackground: pageBackground,
        headerBackground: headerBackground,
        menuBackground: menuBackground,
        startBackground: startBackground,
        navBarCodeBackground: navBarCodeBackground,
        fileManagerBackground: fileManagerBackground,
        fileTabsSelectedBackground: fileTabsSelectedBackground,
        fileTabsUnSelectedBackground: fileTabsUnSelectedBackground,
        newFileCodeCaseBackground: newFileCodeCaseBackground,
        caseBuildCarouselArrowsBackground: caseBuildCarouselArrowsBackground,
        caseBuildHeaderBackground: caseBuildHeaderBackground,
        caseBuildContentBackground: caseBuildContentBackground,
        caseBuildFooterBackground: caseBuildFooterBackground,
        caseCodeStartButtonsBackground: caseCodeStartButtonsBackground,
        darkModeTextColor: darkModeTextColor,
        dotModalHeaderBackground: dotModalHeaderBackground
    };

    return (
        <ColorThemeContext.Provider value = {context}>
            {children}
        </ColorThemeContext.Provider>
    );
}

export default ColorThemeContext;